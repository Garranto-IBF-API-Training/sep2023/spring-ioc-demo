package com.garranto.config;

import com.garranto.model.Account;
import com.garranto.model.Address;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;

@Configuration
public class BeanConfiguration {


    @Bean(value = "accountOne")
    public Account accountOne() {
        Account account = new Account("AC00999", "Loan", 23000);
        return account;

    }

    @Bean(value = "accountTwo")
    public Account accountTwo() {
        return new Account("AC00888", "Current", 5000);
    }

    @Bean
    public Address addressOne() {
        return new Address("MG Street", "Karnataka", "India");
    }

    @Bean
    public Address defaultAddress() {
        return new Address();
    }

}

// <bean id="accountOne" class="com.garranto.model.Account">
//
//    </bean>

//bean referencing
// autowiring

//bean referencing

//   @Bean(value = "accountOne")
//    public Account accountOne(Address addressOne) {
//        Account account = new Account("AC00999", "Loan", 23000);
//        account.setAddress(addressOne);
//        return account;
//
//    }

//annotations
//complexities associated spring (more dependecies, more configurations)
//spring boot
//build rest
//best practices (security, jwt authentication)
