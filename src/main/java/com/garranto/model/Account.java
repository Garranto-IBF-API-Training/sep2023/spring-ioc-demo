package com.garranto.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Account {

    private String accountID;
    private String accountType;

    private double balance;

    @Autowired
    @Qualifier(value = "defaultAddress")
    private Address address;

    public Account(String accountID, String accountType, double balance) {
        this.accountID = accountID;
        this.accountType = accountType;
        this.balance = balance;
        System.out.println("Paramterized constructor is used");
    }

    public Account(){
        System.out.println("Default constructor is used");
    }

//    read access
//    write access


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountID='" + accountID + '\'' +
                ", accountType='" + accountType + '\'' +
                ", balance=" + balance +
                ", address=" + address +
                '}';
    }
}
