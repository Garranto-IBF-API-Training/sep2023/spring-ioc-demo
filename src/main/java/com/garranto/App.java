package com.garranto;

import com.garranto.config.BeanConfiguration;
import com.garranto.model.Account;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Sring Beans Demo");
        System.out.println("=====================");

//        creating the context with configuration metadata present in an xml file located in the classpath
//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

//         creating the context with configuration metadata present in a class file

        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfiguration.class);


        Account accountOne = (Account) context.getBean("accountOne");
        Account accountTwo =  context.getBean("accountTwo",Account.class);
        System.out.println(accountOne);
        System.out.println(accountTwo);

    }
}

// Account accountOne = new Account();
//        Account accountTwo = new Account("ACC002","Savings",1200.01);
//        accountTwo.setBalance(1500.01);
////        accountOne.setAccountID("ACC001");
////        accountTwo.setAccountID("ACC002");
//        System.out.println(accountOne);
//        System.out.println(accountTwo);

//xml
//java
//annotations(declarative way)
//
